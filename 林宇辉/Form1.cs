﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 林宇辉1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static readonly Dictionary<string, string> _user = new Dictionary<string, string>();

        private void button2_Click(object sender, EventArgs e)
        {

            Form2 form2 = new Form2();
            form2.Show();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;
            var res = _user.Where(x=>x.Key==uid).FirstOrDefault();
            if (res.Key==uid && res.Value==pwd) {
                MessageBox.Show("登陆成功","恭喜你",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else {
                MessageBox.Show("登陆失败,请注册","取消",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
            }
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
         
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();
            list.Add(Color.White);
            list.Add(Color.Black);
            list.AddRange(new Color[] {
                Color.Red,Color.Blue,Color.Yellow,Color.Orange
            });
            Random random = new Random();
            var rndInt = random.Next(0, list.Count);
            var rndColor = list[rndInt];
            BackColor = rndColor;
            MessageBox.Show("你咋又变色了");

        }

        public void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("admin", "123456");
        }
    }
}
