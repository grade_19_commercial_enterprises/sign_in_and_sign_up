﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Register : Form
    {
        public Register()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uNe = userName.Text;
            var pWd = passWard.Text;
            var cPWd = confirmPassword.Text;


            var isUneNull = string.IsNullOrEmpty(uNe);
            var isPwdNull = string.IsNullOrEmpty(pWd);
            var isEqure = pWd == cPWd;

            if (!isUneNull  && !isPwdNull  && isEqure)
            {
                MessageBox.Show("注册成功", "请健康游玩");
                Form1._user.Add(uNe, pWd);
                this.Close();

            }
            else 
            {
                MessageBox.Show("注册失败", "请重新注册");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
           
        }
    }
}
