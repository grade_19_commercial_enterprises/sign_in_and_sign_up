﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace winformTest0003
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public Dictionary<string, string> User { get; } = new Dictionary<string, string>;
        
        ;

        public Dictionary<string, string> User1 => User;

        private void Form1_Load(object sender, EventArgs e)
        {
            User.Add("admin", "123456");
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();

            list.Add(Color.Black);

            list.Add(Color.Yellow);

            list.Add(Color.White);

            list.AddRange(new Color[]
            {
                Color.Red,Color.Blue,Color.Yellow,Color.Green,Color.Pink,Color.Orange,Color.Purple,
            });

            Random random = new Random();

            var rndInt = random.Next(0, list.Count);

            var rndColor = list[rndInt];

            BackColor = rndColor;
        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("我想打死你哈哈哈，你要咋地");

        }

        private void Form1_BackColorChanged(object sender, EventArgs e)
        {
            MessageBox.Show("你还么被打死啊","打不死的人");



        }

       // private void Form1_MouseDown(object sender, MouseEventArgs e)
        //{
           // MessageBox.Show("当鼠标按下发生啥啊");

           
       // }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            MessageBox.Show("当鼠标按下时，发生了又以后见晒");
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        //private void textBox2_TextChanged(object sender, EventArgs e)
        //{

        //}

        private void button1_Click(object sender, EventArgs e)
        {
           

            var uid = userName.Text;
            var pwd = passWord.Text;


            var res = User.Where(x => x.Key == uid).FirstOrDefault();

            if(res.Key==uid && res.Value==pwd )
            {
                MessageBox.Show("登录成功", "恭喜你", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("登录失败,你可以重新注册", "很遗憾", MessageBoxButtons.YesNo,  MessageBoxIcon.Warning);
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
