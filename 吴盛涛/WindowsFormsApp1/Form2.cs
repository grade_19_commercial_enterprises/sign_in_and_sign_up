﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        
        //button2的单机事件
        private void button2_Click(object sender, EventArgs e)
        {
            //记录输入的内容
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rpwd = rpassWord.Text;

            //判断
            var isuidnull = string.IsNullOrEmpty(uid);
            var ispwdnull = string.IsNullOrEmpty(pwd);
            var isequre = pwd == rpwd;

            //判断注册是否成功
            if (!isuidnull && !ispwdnull && isequre)
            {
                MessageBox.Show("注册成功！！！");
                Form1.user.Add(uid, pwd);
                //注册成功后关闭窗体
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败！！！");
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //取消注册关闭注册窗体
            this.Close();
        }
    }
}
