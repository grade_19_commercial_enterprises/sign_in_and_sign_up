﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //定义方法
        public static Dictionary<string, string> user = new Dictionary<string, string>();

        private void Form1_Load(object sender, EventArgs e)
        {
            user.Add("admin", "123456");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Textbox输入的内容
            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = user.Where(x => x.Key == uid).FirstOrDefault();

            //判断是否登录成功
            if (res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("恭喜你","登录成功",MessageBoxButtons.YesNoCancel,MessageBoxIcon.None);

                //登录成功后关闭窗体
                this.Close();
            }
            else
            {
                MessageBox.Show("登录失败", "很遗憾", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //实例化变量
            Form2 form = new Form2();
            //跳转到注册页面
            form.Show();
        }
    }
}
