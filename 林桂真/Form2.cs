﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        private object uid;

        public Form2()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = useName.Text;
            var pwd = passWord.Text;
            var rpwd = treTryPassWord.Text;
            var isUidNull = string.IsNullOrEmpty(uid);
            var ispwdNull = string.IsNullOrEmpty(pwd);
            if (!isUidNull && !ispwdNull && pwd == rpwd)
            {
                MessageBox.Show("注册成功", "恭喜！", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else {
                MessageBox.Show("注册失败", "请重新输入", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }
        }
    }
}
