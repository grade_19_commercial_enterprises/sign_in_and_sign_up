﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }
        
        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void maskedTextBox2_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = UserName.Text;

            var pwd = PassWord.Text;

            var rPwd = reTryPassWord.Text;

            var isUidNull = string.IsNullOrEmpty(uid);

            var isPwdNull = string.IsNullOrEmpty(pwd);

            var isEqure = pwd == rPwd;

            if(!isUidNull && !isPwdNull && isEqure)
            {
                MessageBox.Show("恭喜您，注册成功！");
                Form1._user.Add(uid, pwd);
                
            }
            else
            {
                MessageBox.Show("很抱歉，未注册成功！");
            }
            this.Close();
        }
    }
}
