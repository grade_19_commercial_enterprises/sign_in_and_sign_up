﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private readonly Dictionary<string, string> _user = new Dictionary<string, string>
        {

        };

        private void Form1_Load_1(object sender, EventArgs e)
        {
            _user.Add("admin", "123456");
        }

        private void Form1_MouseClick_1(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();
            list.Add(Color.White);
            list.Add(Color.Yellow);
            list.AddRange(new Color[] {
                Color.Red,Color.Pink

            });

            Random random = new Random();
            var rndInt = random.Next(0, 4);

            var rndColor = list[rndInt];

            BackColor = rndColor;
        }

        private void Form1_MouseDoubleClick_1(object sender, MouseEventArgs e)
        {
            MessageBox.Show("你不懂");
        }

        private void Form1_BackColorChanged_1(object sender, EventArgs e)
        {
            MessageBox.Show("你又变色了", "变色系列");
        }


        private void Form1_MouseUp_1(object sender, MouseEventArgs e)
        {
            MessageBox.Show("鼠标弹起，发生另一种事");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
          

            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if (res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("登入成功");
            }
            else
            {
                MessageBox.Show("登入失败");
            }
        }


        private void button2_Click_1(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show("");
        }

        }
    }


