﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindFormtest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       public static Dictionary<string, string> _user = new Dictionary<string, string> { };
            
        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("admin", "20010109");
            //this.BackColor = Color.BurlyWood;
           // MessageBox.Show("hello,窗体","欢迎光临");
            HelpButton = true;
            MinimizeBox = false;
            MaximizeBox = false;
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            List<Color> list = new List<Color>();
            list.Add(Color.Chartreuse);
            list.Add(Color.Blue);
            list.Add(Color.Brown);
            list.AddRange(new Color[] { Color.Red, Color.Pink,
                Color.Snow, Color.YellowGreen });
            Random random = new Random();
            var radint = random.Next(0, 8);
            var radcolor = list[radint];
            BackColor = radcolor;

        }

        private void Form1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            MessageBox.Show("你双击我干嘛？？在动我试试");
        }

        private void Form1_BackColorChanged(object sender, EventArgs e)
        {
            //MessageBox.Show("你怎么变色了", "变色龙系列");
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            var uid = username.Text;
            var pwd = passwad.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();
     
            if (res.Key==uid && res.Value==pwd) {

                MessageBox.Show("登入成功", "恭喜你",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else{
            MessageBox.Show("登入失败,请重新注册","很遗憾",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }

      

      
       
    }
}
