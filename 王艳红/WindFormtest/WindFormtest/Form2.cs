﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindFormtest
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btn_Click(object sender, EventArgs e)
        {
            var uid = username.Text;
            var pwd = passwad.Text;
            var rewd = retrypasswad.Text;
            var isuidnull = string.IsNullOrEmpty(uid);
            var ispwdnull = string.IsNullOrEmpty(pwd);
            var isequre = pwd == rewd;

            if (!isuidnull && !ispwdnull && isequre)
            {

                MessageBox.Show("注册成功！");
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else {

                MessageBox.Show("注册未成功！！");
            }

        }

        
    }
}
