﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTest
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = userPwd.Text;
            var pwd2 = userPwd2.Text;
            var usersNaem = !string.IsNullOrEmpty(uid);
            var usersPwd = !string.IsNullOrEmpty(pwd);
            var usersPwd2 = pwd == pwd2;
            if (usersNaem && usersPwd && usersPwd2)
            {
                MessageBox.Show("注册成功");
                Form1.users.Add(uid, pwd);
                Form1 form1 = new Form1();
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败");
            }
        }
    }
}
