﻿using SolrNet.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinFormTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<String, String> users = new Dictionary<string, string>();

        private void button2_Click(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();
            registerForm.Show();
        }


        //登录
        private void button1_Click(object sender, EventArgs e)
        {
            var user = UserName.Text;
            var pwd = UserPwd.Text;
            var usr = users.Where(x => x.Key == user && x.Value == pwd)
                .Select(x => new { key = x.Key, value = x.Value }).FirstOrDefault();
            if (usr != null)
            {

                MessageBox.Show("登录成功");
                Form2 form2 = new Form2();
                form2.Show();
            }
            else
            {
                MessageBox.Show("登录失败");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            users.Add("jx","123456");
        }
    }
}
