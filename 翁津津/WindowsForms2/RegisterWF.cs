﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms2
{
    public partial class RegisterWF : Form
    {
        public RegisterWF()
        {
            InitializeComponent();
        }

        private void register_Click(object sender, EventArgs e)
        {
            var uid = username.Text;
            var pwd = pad.Text;
            var rpad = retrypad.Text;

            var isuidnull = string.IsNullOrEmpty(uid);
            var ispwdnull = string.IsNullOrEmpty(pwd);
            var isequre = pwd == rpad;

            if (!isuidnull && !ispwdnull && isequre)
            {
                MessageBox.Show("注册成功！！！");
                Form1._user.Add(uid, pwd);
                
            }
            else
            {
                MessageBox.Show("注册失败！！！");
            }
        }
    }
}
