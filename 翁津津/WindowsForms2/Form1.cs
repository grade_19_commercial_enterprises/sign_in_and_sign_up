﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForms2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> _user = new Dictionary<string, string> { };

        //方法
        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("yee","1128");
            /*
            //设置背景色
            this.BackColor = Color.Yellow;

            //设置消息框
            MessageBox.Show("求放假！！");
            */

            //帮助按钮
            HelpButton = true;
            //去掉最小最大化，帮助按钮出现
            MinimizeBox = false;
            MaximizeBox = false;
        }
        
        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            /*
            //泛型：表明这个集合里都是color这个类型的东东
            List<Color> list = new List<Color>();
            //一个一个添加
            list.Add(Color.Red);
            list.Add(Color.Blue);
            //批量添加
            list.AddRange(new Color[]
            {
            Color.Gray,Color.Pink,Color.Purple,Color.Orange
            });
            Random ran = new Random();
            var rndInt = ran.Next(0,list.Count);
            var rndColor = list[rndInt];
            BackColor = rndColor;
            */
        }
        
        //当背景色改变发生的事件方法
        private void Form1_BackColorChanged(object sender, EventArgs e)
        {/*
            MessageBox.Show("终于要放假了！！！");
            */
        }

        //登录事件的方法
        private void button2_Click(object sender, EventArgs e)
        {
            //定义一个用户名和密码
            //var name = "Jackson.yee";
            //var password = "20001128";

            var uid = username.Text;
            var pwd = pad.Text;

            var res = _user.Where(x=>x.Key==uid).FirstOrDefault();

            if (res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("登录成功！","消息弹出",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            }
            else 
            {
                MessageBox.Show("登录失败，请重试！","消息弹出",MessageBoxButtons.YesNo,MessageBoxIcon.Error);
            }
        }
        //注册事件的方法
        private void button1_Click(object sender, EventArgs e)
        {
            RegisterWF wF = new RegisterWF();
            wF.Show();
        }
    }
}
