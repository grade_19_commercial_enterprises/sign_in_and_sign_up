﻿namespace WindowsForms2
{
    partial class RegisterWF
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pad = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.username = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnpad = new System.Windows.Forms.Button();
            this.register = new System.Windows.Forms.Button();
            this.retrypad = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pad
            // 
            this.pad.Location = new System.Drawing.Point(241, 132);
            this.pad.Name = "pad";
            this.pad.Size = new System.Drawing.Size(207, 21);
            this.pad.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(154, 137);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "密  码：";
            // 
            // username
            // 
            this.username.Location = new System.Drawing.Point(241, 72);
            this.username.Name = "username";
            this.username.Size = new System.Drawing.Size(207, 21);
            this.username.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(154, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 4;
            this.label1.Text = "用户名：";
            // 
            // btnpad
            // 
            this.btnpad.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnpad.Location = new System.Drawing.Point(344, 235);
            this.btnpad.Name = "btnpad";
            this.btnpad.Size = new System.Drawing.Size(75, 33);
            this.btnpad.TabIndex = 9;
            this.btnpad.Text = "取消";
            this.btnpad.UseVisualStyleBackColor = true;
            // 
            // register
            // 
            this.register.Font = new System.Drawing.Font("宋体", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.register.Location = new System.Drawing.Point(166, 235);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(75, 33);
            this.register.TabIndex = 8;
            this.register.Text = "注册";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // retrypad
            // 
            this.retrypad.Location = new System.Drawing.Point(241, 190);
            this.retrypad.Name = "retrypad";
            this.retrypad.Size = new System.Drawing.Size(207, 21);
            this.retrypad.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(154, 195);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "确定密码：";
            // 
            // RegisterWF
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.retrypad);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnpad);
            this.Controls.Add(this.register);
            this.Controls.Add(this.pad);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.username);
            this.Controls.Add(this.label1);
            this.Name = "RegisterWF";
            this.Text = "vv";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox pad;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox username;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnpad;
        private System.Windows.Forms.Button register;
        private System.Windows.Forms.TextBox retrypad;
        private System.Windows.Forms.Label label3;
    }
}