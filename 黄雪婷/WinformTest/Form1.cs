﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinformTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> users = new Dictionary<string, string>();

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwd = textBox2.Text;


            var usr = users.Where(x => x.Key == uid && x.Value == pwd)
                .Select(x => new { key = x.Key, value = x.Value }).FirstOrDefault();

          

            if(usr!= null)
            {
                MessageBox.Show("登录成功");
                MainForm mainForm = new MainForm();
                this.Hide();
                mainForm.Show();

  
            }
            else
            {
                MessageBox.Show("登录失败");
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            users.Add("YanYan", "123456");
     
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RegisterForm registerForm = new RegisterForm();

            registerForm.Show();
        
    }
    }
}
