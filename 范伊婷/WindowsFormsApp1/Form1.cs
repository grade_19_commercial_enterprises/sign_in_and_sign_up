﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> _user = new Dictionary<string, string>();
        private void Form1_Load(object sender, EventArgs e)
        {
            this.BackColor = Color.Pink;
            _user.Add("1234", "11111");

        }

        private void button1_Click(object sender, EventArgs e)
        {
          

            var uid = userName.Text;
            var pwd = passWord.Text;

            var res = _user.Where(x => x.Key == uid).FirstOrDefault();

            if(res.Key==uid && res.Value==pwd)
            {
                MessageBox.Show("登录成功", "恭喜你",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("登录失败", "很遗憾",MessageBoxButtons.YesNo,MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }
    }
}
