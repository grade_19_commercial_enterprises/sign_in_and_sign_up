﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp4
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = reTryPassWord.Text;

            var IsUidNull = string.IsNullOrEmpty(uid);
            var IsPwdNull = string.IsNullOrEmpty(pwd);
            var isEquer = pwd == rPwd;

            if (!IsUidNull && !IsPwdNull && isEquer)
            {
                MessageBox.Show("注册成功", "恭喜你");
                Form1._user.Add(uid, pwd);
            }
            else
            {
                MessageBox.Show("注册未成功", "很遗憾");
                
            }
            this.Close();
        }
    }
}
