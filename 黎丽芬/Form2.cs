﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_and_Registration
{
    public partial class Form2 : Form
    {
        

        public Form2()
        {
            InitializeComponent();
        }

        private void Registration_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = rePassWord.Text;

            var isUidNull = string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqual = pwd == rPwd;



            if (!isUidNull && !isPwdNull&& isEqual)
            {
                MessageBox.Show("注册成功");
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败");
            }
        }
    }
}
