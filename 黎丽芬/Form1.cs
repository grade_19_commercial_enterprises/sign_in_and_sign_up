﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Login_and_Registration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public static Dictionary<string, string> _user = new Dictionary<string, string>();

        private void button1_Click(object sender, EventArgs e)
        {
           
            var uid = userName.Text;
            var pwd = passWord.Text;
            var res = _user.Where(x => x.Key == uid).FirstOrDefault();
           
            if(res.Key ==uid && res.Value ==pwd )
            {
                MessageBox.Show("登陆成功");
            }
            else
            {
                MessageBox.Show("用户名或密码输入错误");
            }
   
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            _user.Add("sa", "123");

            //userName .Focus();

            //foreach (Control control in this.Controls)
            //{
            //    if(control is TextBox)
            //    {
            //        passWord  = control as TextBox;
            //        control.KeyDown += new KeyEventHandler(Key_Down);
            //    }
            //}
        }

        //private void Key_Down(object sender, KeyEventArgs e)
        //{
        //    if(e.KeyCode == Keys.Enter)
        //    {
        //        passWord.Focus();
        //    }
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
        }

        private void userName_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                passWord.Focus();
            }
        }
    }
}
