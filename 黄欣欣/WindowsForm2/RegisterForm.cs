﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsForm2
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rPwd = reTrypassword.Text;


            var isUidNull = string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqual = pwd == rPwd;
            
            if (!isUidNull && !isPwdNull && isEqual)
            {
                MessageBox.Show("恭喜你", "注册成功");
                Form1._user.Add(uid, pwd);
                this.Close();

        
            }
            else
            {
                MessageBox.Show("很抱歉", "注册未成功");
            }
               

            }
        }
    }

