﻿namespace Project
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.userName = new System.Windows.Forms.TextBox();
            this.passPwd = new System.Windows.Forms.TextBox();
            this.btnRsgister = new System.Windows.Forms.Button();
            this.btnCanceRegister = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.reTypassWord = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // userName
            // 
            this.userName.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.userName.Location = new System.Drawing.Point(279, 66);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(319, 36);
            this.userName.TabIndex = 0;
            // 
            // passPwd
            // 
            this.passPwd.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.passPwd.Location = new System.Drawing.Point(279, 125);
            this.passPwd.Name = "passPwd";
            this.passPwd.Size = new System.Drawing.Size(319, 36);
            this.passPwd.TabIndex = 0;
            // 
            // btnRsgister
            // 
            this.btnRsgister.Cursor = System.Windows.Forms.Cursors.No;
            this.btnRsgister.Font = new System.Drawing.Font("华文楷体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnRsgister.Location = new System.Drawing.Point(154, 293);
            this.btnRsgister.Name = "btnRsgister";
            this.btnRsgister.Size = new System.Drawing.Size(150, 59);
            this.btnRsgister.TabIndex = 1;
            this.btnRsgister.Text = "注 册";
            this.btnRsgister.UseVisualStyleBackColor = true;
            this.btnRsgister.Click += new System.EventHandler(this.btnRsgister_Click);
            // 
            // btnCanceRegister
            // 
            this.btnCanceRegister.Font = new System.Drawing.Font("华文楷体", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnCanceRegister.Location = new System.Drawing.Point(436, 293);
            this.btnCanceRegister.Name = "btnCanceRegister";
            this.btnCanceRegister.Size = new System.Drawing.Size(162, 59);
            this.btnCanceRegister.TabIndex = 1;
            this.btnCanceRegister.Text = "取消注册";
            this.btnCanceRegister.UseVisualStyleBackColor = true;
            this.btnCanceRegister.Click += new System.EventHandler(this.button2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("华文楷体", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(134, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "用户名：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("华文楷体", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(134, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 31);
            this.label2.TabIndex = 2;
            this.label2.Text = "密   码：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("华文楷体", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(50, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "再次确认密码：";
            // 
            // reTypassWord
            // 
            this.reTypassWord.Font = new System.Drawing.Font("宋体", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.reTypassWord.Location = new System.Drawing.Point(279, 197);
            this.reTypassWord.Name = "reTypassWord";
            this.reTypassWord.Size = new System.Drawing.Size(319, 36);
            this.reTypassWord.TabIndex = 0;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCanceRegister);
            this.Controls.Add(this.btnRsgister);
            this.Controls.Add(this.reTypassWord);
            this.Controls.Add(this.passPwd);
            this.Controls.Add(this.userName);
            this.Name = "Form2";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox userName;
        private System.Windows.Forms.TextBox passPwd;
        private System.Windows.Forms.Button btnRsgister;
        private System.Windows.Forms.Button btnCanceRegister;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox reTypassWord;
    }
}