﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void  Button1_Click(object sender, EventArgs e)
        {
            var uid = uName.Text;
            var pwd = pWord.Text;
            var rpwd = rpWord.Text;
            var isuidNull= string.IsNullOrEmpty(uid);
            var isPwdNull = string.IsNullOrEmpty(pwd);
            var isEqure = pwd == rpwd;
            if(!isuidNull && !isPwdNull && isEqure)
            {
                MessageBox.Show("注册成功！","完成");
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else
            {
                MessageBox.Show("注册失败，重新注册", "抱歉");

            }
        }


    }
}
