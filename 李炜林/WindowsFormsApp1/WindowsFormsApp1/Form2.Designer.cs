﻿namespace WindowsFormsApp1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.name = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.Label();
            this.affirmpwd = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.register = new System.Windows.Forms.Button();
            this.Unregister = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.name.Location = new System.Drawing.Point(156, 55);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(129, 29);
            this.name.TabIndex = 0;
            this.name.Text = "用户名：";
            // 
            // password
            // 
            this.password.AutoSize = true;
            this.password.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.password.Location = new System.Drawing.Point(156, 144);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(100, 29);
            this.password.TabIndex = 0;
            this.password.Text = "密码：";
            // 
            // affirmpwd
            // 
            this.affirmpwd.AutoSize = true;
            this.affirmpwd.Font = new System.Drawing.Font("宋体", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.affirmpwd.Location = new System.Drawing.Point(156, 224);
            this.affirmpwd.Name = "affirmpwd";
            this.affirmpwd.Size = new System.Drawing.Size(158, 29);
            this.affirmpwd.TabIndex = 0;
            this.affirmpwd.Text = "确认密码：";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(326, 63);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(262, 21);
            this.textBox1.TabIndex = 1;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(326, 152);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(262, 21);
            this.textBox2.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(326, 232);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(262, 21);
            this.textBox3.TabIndex = 1;
            // 
            // register
            // 
            this.register.Location = new System.Drawing.Point(161, 306);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(115, 39);
            this.register.TabIndex = 2;
            this.register.Text = "注册";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // Unregister
            // 
            this.Unregister.Location = new System.Drawing.Point(473, 306);
            this.Unregister.Name = "Unregister";
            this.Unregister.Size = new System.Drawing.Size(115, 39);
            this.Unregister.TabIndex = 2;
            this.Unregister.Text = "取消注册";
            this.Unregister.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Unregister);
            this.Controls.Add(this.register);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.affirmpwd);
            this.Controls.Add(this.password);
            this.Controls.Add(this.name);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Label password;
        private System.Windows.Forms.Label affirmpwd;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button register;
        private System.Windows.Forms.Button Unregister;
    }
}