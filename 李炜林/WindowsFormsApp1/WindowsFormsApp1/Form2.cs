﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void register_Click(object sender, EventArgs e)
        {
            var uid = textBox1.Text;
            var pwds = textBox2.Text;
            var rpwd = textBox3.Text;

            var isUidnull = string.IsNullOrEmpty(uid);
            var ispwdsnull = string.IsNullOrEmpty(pwds);
            var isEqure = pwds == rpwd;

            if (!isUidnull && !ispwdsnull && isEqure)
            {
                MessageBox.Show("恭喜你注册成功！");
                Form1._user.Add(uid, pwds);
                this.Close();


            }
            else
            {
                MessageBox.Show("很遗憾注册失败！");

            }





        }
    }
}
