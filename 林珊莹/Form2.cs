﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace praticeWindows
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var uid = userName.Text;
            var pwd = passWord.Text;
            var rpwd = NewpassWord.Text;
            var IsuidNull = string.IsNullOrEmpty(uid);
            var IspwdNull = string.IsNullOrEmpty(pwd);
            var Equale = pwd == rpwd;
            if (!IsuidNull && !IspwdNull && Equale)
            {
                MessageBox.Show("恭喜您，注册成功", "信息提示框", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                Form1._user.Add(uid, pwd);
                this.Close();
            }
            else 
            {
                MessageBox.Show("很遗憾，注册失败", "信息提示框", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            }
        }
    }
}
